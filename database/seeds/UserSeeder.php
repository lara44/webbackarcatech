<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $users = array(
            [
                'name' => 'Luis Alberto',
                'email' => 'adames.lancero@gmail.com',
                'password' => \Hash::make('123456')
            ]
        );

        foreach($users as $item){
            User::updateOrCreate($item);
        }
    }
}
