<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    //
    public function scopeSearch($query, $search){
        return $query
            ->where('title','LIKE','%'.$search.'%')
            ->orwhere('description','LIKE','%'.$search.'%');
    }
}
