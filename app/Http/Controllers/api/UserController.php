<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return response()->json(['users'=> $users], 200);
    }

    public function store(Request $request){
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make('123456789');
            $user->save();
            return response()->json(['user' => $user], 201);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], 501);
        }
    }

    public function update(Request $request){
        try {
            $user = User::where('id', $request->id)->first();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make('123456789');
            $user->save();
            return response()->json(['user' => $user], 201);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], 501);
        }
    }
}
