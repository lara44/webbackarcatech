<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Exam;

class ExamController extends Controller
{
    public function index(Request $request) {
        $search = $request->search;
        $exams = Exam::search($search)->paginate(12);
        return response()->json([
            'message'=> 'success',
            'data' => $exams
        ], 201);
    }

    public function store(Request $request){
        try {
            $exam = new Exam();
            $exam->title = $request->title;
            $exam->description = $request->description;
            $exam->timehour = $request->timehour;
            $exam->attempts = $request->attempts;
            $exam->save();
            return response()->json([
                'message'=> 'success',
                'data' => $exam
            ], 201);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], 501);
        }
    }

    public function update(Request $request){
        try {
            $exam = Exam::find($request->id);
            $exam->title = $request->title;
            $exam->description = $request->description;
            $exam->timehour = $request->timehour;
            $exam->attempts = $request->attempts;
            $exam->save();
            return response()->json([
                'message'=> 'success',
                'data' => $exam
            ], 201);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], 501);
        }
    }
}
